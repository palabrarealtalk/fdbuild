# SPDX-FileCopyrightText: 2020 Roman Gilg <subdiff@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later


class Stage:
    def __init__(self, ident, step):
        self.identifier = ident
        self.step = step
