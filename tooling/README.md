<!--
SPDX-FileCopyrightText: 2022 Roman Gilg <subdiff@gmail.com>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# FDBuild Tooling
## Documentation
Uses the commit message lint functionality from the [tooling repo][kwinft-tooling].
See there for information on how the commitlint tool can be run locally too.

## Releases
Uses [commitizen](https://github.com/commitizen-tools/commitizen) for bumping version and
generating the changelog and standard Python packaging tooling for building and uploading
to PyPi as described [here](https://packaging.python.org/en/latest/tutorials/packaging-projects/).
And in the end don't forget to push the bump commit and new tag to the repo.

Together this means to issue the following commands (uploads first to PyPi test instance):

```
cz bump
python -m build
python -m twine upload --repository testpypi dist/*
python -m twine upload dist/*
git push --follow-tags
```

[kwinft-tooling]: https://gitlab.com/kwinft/tooling
