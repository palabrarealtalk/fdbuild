# Lints messages of all commits between current branch and target branch.
# Variables:
# * COMMITLINT_CONFIG_PATH: Path to the commitlint.config.js file, required by commitlint.
# * TARGET_BRANCH: The reference branch we filter additional commits by.
.message-lint:
  variables:
    COMMITLINT_CONFIG_PATH: tooling/docs/commitlint.config.js
    TARGET_BRANCH: master
  image: node:latest
  rules:
    - if: '$COVERITY_SCAN_RUN'
      when: never
    - if: $CI_MERGE_REQUEST_IID
      when: on_success
    - if: '$CI_COMMIT_BRANCH == $TARGET_BRANCH || $CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^Plasma\//'
      when: never
    - when: on_success
  script:
    - UPSTREAM_REPO="https://${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}@${CI_SERVER_HOST}/kwinft/${CI_PROJECT_NAME}.git"
    # To unshallow from the branch in the fork we need to ensure source branch and origin are set accordingly.
    - if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" ];
      then SOURCE_BRANCH=$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME;
           git remote set-url origin "https://${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}@${CI_SERVER_HOST}/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH}.git";
      else SOURCE_BRANCH=$CI_COMMIT_BRANCH; fi
    - if [ -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ];
      then export COMPARE_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME; else export COMPARE_BRANCH=$TARGET_BRANCH; fi
    - "echo Source branch: $SOURCE_BRANCH"
    - "echo Compare branch: $COMPARE_BRANCH"
    # Required as commitlint since 17.1.1 otherwise fails to load our js configs.
    # TODO(romangg): Remove again once fixed.
    - yarn global add ts-node
    - yarn global add @commitlint/cli
    - yarn add conventional-changelog-conventionalcommits
    # If the target head doesn't exist in our checkout we must unshallow the checkout (in GitLab
    # pipelines a source checkout is by default only 20-50 commits deep).
    - git fetch --depth=1 origin ${TARGET_BRANCH}
    - TARGET_HEAD_HASH=$(git rev-parse origin/${TARGET_BRANCH})
    - if [ ! "$(git branch --contains $TARGET_HEAD_HASH)" ];
      then git fetch --unshallow origin $SOURCE_BRANCH; fi
    - git remote add _upstream $UPSTREAM_REPO || git remote set-url _upstream $UPSTREAM_REPO
    - git fetch -q _upstream $COMPARE_BRANCH
    - commitlint --verbose --config=$COMMITLINT_CONFIG_PATH --from=_upstream/$COMPARE_BRANCH
  cache:
    paths:
      - node_modules
