<!--
SPDX-FileCopyrightText: 2023 Roman Gilg <subdiff@gmail.com>

SPDX-License-Identifier: GPL-3.0-or-later
-->

## v0.3.0 (2024-02-21)

### Feat

- add support for CMake presets

## v0.2.0 (2023-12-30)

### Feat

- **template**: add media template
- **template**: enable OpenCL options
- **template**: add QCoro
- **template**: add xf86-input-evdev project
- **template**: add at-spi2-core to qt6
- **template**: add libpciaccess
- **template**: use meson for xorgproto
- **template**: pull Qt 6 from official Qt mirror

### Fix

- **template**: set common global environment variables in default templates
- read envvars from parent
- **template**: add libva and libvdpau
- **template**: remove wlroots from linux-desktop-meta
- reserve "envvars" key

### Refactor

- **template**: rename xorg-macros project
